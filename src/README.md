# WORD MASTER GAME

This project is a replica of a online game Wordle. It is one type of challenging game.

Guessing the word.

## Used technologies
---
<ul>
    <li>Html is used to made the structure how to appear.</li>
    <li>CSS is used to add styles.</li>
    <li>JavaScript and DOM manipulations is used to make interactive.</li>
</ul>

## CHALLENGES
---
<ul>
    <li>To create boxes and align them together.</li>
    <li>Taking input from user and comparing with obtained word.</li>
    <li>Display them in a proper manner.</li>
    <li>Fetching to get the word.</li>
</ul>

## Visit this site to play this game
---

URL : https://friendly-moxie-913500.netlify.app