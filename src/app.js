import { spinner,checkingAndComparing,addLetter,backSpace,isLetter,afterGettingOutput } from "./BussinessLogic.js";

function fetchingAndAddingEventListener() {

    document.addEventListener('keydown', function (event) {
        if(afterGettingOutput){
            return;
        }
        let action = event.key;
        if (action === 'Enter') {
            spinner(false)
            checkingAndComparing();
        } else if (action === "Backspace") {
            backSpace()
        } else if (isLetter(action)) {
            addLetter(action.toUpperCase());
        }
    });
}

fetchingAndAddingEventListener();